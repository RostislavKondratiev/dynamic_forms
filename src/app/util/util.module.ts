import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InputComponent } from './components/forms/input/input.component';
import { SelectComponent } from './components/forms/select/select.component';
import { SelectModule } from 'ng-select';
import { TableComponent } from './components/table/table.component';
import { TableRowComponent } from './components/table/table-row/table-row.component';
import { TableFiltersComponent } from './components/table/table-filters/table-filters.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginationComponent } from './components/table/pagination/pagination.component';
import { BusyModule } from 'angular2-busy';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TypeaheadComponent } from './components/forms/typeahead/typeahead.component';
import { TestService } from './helpers/test.service';

const providers = [TestService];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        SelectModule,
        BrowserAnimationsModule,
        BusyModule,
        NgbModule,
    ],
    declarations: [
        InputComponent,
        SelectComponent,
        TypeaheadComponent,
        TableComponent,
        TableRowComponent,
        TableFiltersComponent,
        PaginationComponent,
    ],
    providers: [],
    exports: [
        InputComponent,
        SelectComponent,
        TableComponent,
        TypeaheadComponent,
        TableRowComponent,
        TableFiltersComponent,
        PaginationComponent
    ]
})
export class UtilModule {
    public static forRoot(): ModuleWithProviders{
        return {
            ngModule: UtilModule,
            providers: providers  // tslint:disable-line
        };
    }
}