import { Injectable } from '@angular/core';

@Injectable()
export class TestService {
    public val = 3;
    public changeVal(val) {
        this.val += val;
    }
}
