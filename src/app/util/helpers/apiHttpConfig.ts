export class ApiHttpConfig {
    public static base = 'http://localhost:3000';
    public static users = `${ApiHttpConfig.base}/users`;
    public static deals = `${ApiHttpConfig.base}/deals`;
}