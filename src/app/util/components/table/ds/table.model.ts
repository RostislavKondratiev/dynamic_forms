import { ServerDataSource } from './server.dataSource';
/* tslint:disable */
export class TableConfig {
    public filters: TableFilter[];
    public headers: string[];
    public datasource: ServerDataSource;
    public route: string;
    constructor(filters, headers, datasource, route) {
        this.filters = filters;
        this.headers = headers;
        this.datasource = datasource;
        this.route = route;
    }
}
export class TableFilter {
    public sortableKey: string;
    public type: string;
    public label: string;
    public name: string;
    public options: FilterOption[];
    constructor(sortableKey, type, name, label, options = null) {
        this.sortableKey = sortableKey;
        this.type = type;
        this.name = name;
        this.label = label;
        this.options = options;
    }
}
export class FilterOption {
    public label;
    public value;
    constructor(label, value) {
        this.label = label;
        this.value = value;
    }
}
