import { HttpClient } from '@angular/common/http';
import { ApiHttpConfig } from '../../../helpers/apiHttpConfig';
export class ServerDataSource {
    public config;
    constructor(private http: HttpClient, endpoint, pagination, sort, queryParams = false) {
        this.config = {
            endpoint, pagination, sort, queryParams, filters: null
        };
    }
    public request() {
        let params = this.createOptionsUrl();
        let url = `${ApiHttpConfig.base}/${this.config.endpoint}${params}`;
        return this.http.get(url).map((res) => {
            return {
                data: res,
                params: this.config.queryParams ? this.urlParams() : {}
            };
        }).delay(500);
    }
    public get sortStatus() {
        return this.config.sort.status;
    }
    public get paginationStatus() {
        return this.config.pagination.status;
    }
    public addPaginationConfig(config) {
        this.config.pagination = config;
    }
    public addSortConfig(config) {
        this.config.sort = config;
    }
    public changePage(page) {
        this.config.pagination.page = page;
    }
    public getFiltersValue(filtersValue) {
        this.config.filters = filtersValue;
        console.log(this.config.filters);
    }
    public changeSortDirection(sortDirection, target) {
        this.config.sort['direction'] = sortDirection;
        this.config.sort['target'] = target;
    }
    private createOptionsUrl() {
        let res = [];
        console.log(this.config.filters);
        if (this.config.pagination.status && this.config.pagination.page && this.config.pagination.limit) {
            res.push(`_page=${this.config.pagination.page}&_limit=${this.config.pagination.limit}`);
        }
        if (this.config.sort.status && this.config.sort.target && this.config.sort.direction) {
            res.push(`_sort=${this.config.sort.target}&_order=${this.config.sort.direction}`);
        }
        for (let filter in this.config.filters) {
            if (this.config.filters.hasOwnProperty(filter)) {
                if (this.config.filters[filter]) {
                    res.push(`${filter}=${this.config.filters[filter]}`);
                }
            }
        }
        return res.length > 0 ? `?${res.join('&')}` : '';
    }
    private createOptionsHeaders() {
        let params: URLSearchParams = new URLSearchParams();
        if (this.config.pagination.status && this.config.pagination.page && this.config.pagination.limit) {
            params.set('page', this.config.pagination.page);
            params.set('limit', this.config.pagination.limit);
        }
        if (this.config.sort.status && this.config.sort.target && this.config.sort.direction) {
            params.set('sort', this.config.sort.target);
            params.set('order', this.config.sort.direction);
        }
        for (let filter in this.config.filters) {
            if (this.config.filters.hasOwnProperty(filter)) {
                if (this.config.filters[filter]) {
                    params.set(`${filter}`, this.config.filters[filter]);
                }
            }
        }
        return params;
    }
    private urlParams() {
        let res = {};
        if (this.config.sort.status) {
            res['sort'] = this.config.sort.target;
            res['order'] = this.config.sort.direction;
        }
        if (this.config.filters) {
            for (let filter in this.config.filters) {
                if (this.config.filters.hasOwnProperty(filter)) {
                    if (this.config.filters[filter]) {
                        res[filter] = this.config.filters[filter];
                    }
                }
            }
        }
        if (this.config.pagination.status) {
            res['page'] = this.config.pagination.page;
        }
        return res;
    }
}
