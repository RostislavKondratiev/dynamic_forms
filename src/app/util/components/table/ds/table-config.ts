export class TableConfig {
    public static getHeaders() {
        return ['name', 'username', 'email', 'phone', 'website'];
    }
    public static getFields(data) {
        let headers = TableConfig.getHeaders();
        let response = [];
        for (let item of data){
            let obj = {};
            for (let header of headers){
                 obj[header] = item[header];
            }
            response.push(obj);
            }
        return response;
    }
}
