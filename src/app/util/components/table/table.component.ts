import { Component, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { TableService } from './table.service';
import { ServerDataSource } from './ds/server.dataSource';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'dt-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() public tableConfig;
    public data;
    public page;
    public subs: Subscription[] = [];
    public busy = null;
    private lastSorted;
    private sortDirection = null;
    constructor(private router: Router, private route: ActivatedRoute) {
    }
    public ngOnChanges(changes) {
        console.log(changes);
    }
    public ngOnInit() {
        this.subs.push(this.route.queryParams.subscribe((params) => {
            if (this.tableConfig.datasource.paginationStatus) {
                this.page = params.page || 1;
                this.tableConfig.datasource.changePage(params.page || 1);
            }
            if (this.tableConfig.datasource.sortStatus) {
                this.lastSorted = params.sort;
                this.sortDirection = params.order === 'asc';
                this.tableConfig.datasource.changeSortDirection(params.order, params.sort);
            }
            if (this.tableConfig.filters) {
                for (let filter of this.tableConfig.filters) {
                    for (let param of Object.keys(params)) {
                        if (filter.sortableKey === param) {
                            filter.value = params[param];
                        }
                    }
                }
            } else {
                this.getData();
            }
        }));
    }
    public ngOnDestroy() {
        this.subs.forEach((sub) => {
            sub.unsubscribe();
        });
    }
    public sort(target) {
        if (this.tableConfig.datasource.sortStatus) {
            if (this.lastSorted !== target) {
                console.log(11);
                this.sortDirection = true;
                this.lastSorted = target;
                this.tableConfig.datasource.changeSortDirection(this.sortDirection ? 'asc' : 'desc', target);
            } else {
                console.log(222);
                console.log(this.sortDirection);
                this.sortDirection = !this.sortDirection;
                this.tableConfig.datasource.changeSortDirection(this.sortDirection ? 'asc' : 'desc', target);
            }
            this.getData();
        }
    }
    public clearSort() {
        if (this.tableConfig.datasource.sortStatus) {
            this.tableConfig.datasource.changeSortDirection(null, null);
            this.lastSorted = null;
            this.getData();
        }
    }
    public filtersClearSort(){
        this.lastSorted = null;
    }
    public filterChanged($event) {
        console.log(111111);
        this.tableConfig.datasource.getFiltersValue($event);
        this.page = this.tableConfig.datasource.config.pagination.page;
        this.getData();
    }
    public pageChanged($event) {
        this.page = $event;
        this.tableConfig.datasource.changePage($event);
        this.getData();
    }
    public enableSort() {
        this.tableConfig.datasource.addSortConfig({status: true});
    }
    private getData() {
        setTimeout(() => {
            this.busy = this.tableConfig.datasource.request().subscribe((res) => {
                this.router.navigate([this.tableConfig.route], {queryParams: res.params});
                this.data = res.data;
            });
        }, 0);
    }
}
