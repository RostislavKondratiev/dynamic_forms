import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs';

@Component({
    selector: 'dt-filter',
    templateUrl: './table-filters.component.html',
    styleUrls: ['./table-filters.component.scss']
})
export class TableFiltersComponent implements OnChanges, OnInit {
    @Input() public tableConfig;
    @Output() public filtersChange = new EventEmitter<any>();
    @Output() public resetSort = new EventEmitter<any>();
    public filtersGroup: FormGroup;
    public filtersSub: Subscription;
    public ngOnChanges(changes: SimpleChanges) {
        console.log('filters change');
        this.initFilters();
    }
    public ngOnInit() {
        let filtersValue = {};
        for (let filter of this.tableConfig.filters) {
            if (filter.value !== undefined) {
                filtersValue[filter.sortableKey] = filter.value;
            }
        }
        console.log(filtersValue);
        this.filtersChange.emit(filtersValue);
    }
    public initFilters() {
        if (this.filtersSub) {
            this.filtersSub.unsubscribe();
        }
        this.filtersGroup = new FormGroup({});
        this.tableConfig.filters.forEach((filter) => {
            // if (filter.type !== 'typeahead') {
                this.filtersGroup.addControl(filter.sortableKey, new FormControl(filter.value || ''));
            // } else {
            //
            // }
        }
        );
        this.filtersSub = this.filtersGroup.valueChanges.debounceTime(400).subscribe((res) => {
            this.tableConfig.datasource.changePage(1);
            this.filtersChange.emit(res);
        });
    }
    public clearFilters() {
        for (let filter in this.filtersGroup.controls) {
            if (this.filtersGroup.controls.hasOwnProperty(filter)) {
                this.filtersGroup.controls[filter].patchValue('');
            }
        }
        if (this.tableConfig.datasource.paginationStatus) {
           this.tableConfig.datasource.changePage(1);
        }
        if (this.tableConfig.datasource.sortStatus) {
            this.tableConfig.datasource.changeSortDirection(null, null);
            this.resetSort.emit(true);
        }
    }
}
