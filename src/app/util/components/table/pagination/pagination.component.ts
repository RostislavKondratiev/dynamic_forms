import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'dt-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
    @Input() public page;
    @Output() public pageChange = new EventEmitter<number>();
    public clickPage() {
        setTimeout(() => {
            this.pageChange.emit(this.page);
        }, 100);
    }
}
