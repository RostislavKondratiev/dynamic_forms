import { Component, Input, OnChanges } from '@angular/core';

@Component({
    selector: '[my-row]',
    templateUrl: './table-row.component.html',
    styleUrls: ['./table-row.component.scss']
})
export class TableRowComponent implements OnChanges{
    @Input() public data;
    @Input() public headers;
    public ngOnChanges() {
        console.log(this.data);
    }
}
