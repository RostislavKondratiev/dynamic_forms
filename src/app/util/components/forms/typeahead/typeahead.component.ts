import { Component, Input, OnChanges, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'dt-typeahead',
    templateUrl: './typeahead.component.html',
    styleUrls: ['./typeahead.component.scss']
})
export class TypeaheadComponent implements OnInit, OnDestroy {
    @Input() public control: FormControl;
    @Input() public placeholder: string;
    @Input() public type: string = 'text';
    @Input() public name: string;
    @Input() public label: string;
    @Input() public target: string;
    @Input() public style: any;
    @Input() public searchFunction;
    @Input() public template: TemplateRef<any> = null;
    public subscription: Subscription;
    public data;
    public ctx: TypeaheadComponent;
    public ngOnInit() {
        this.control = this.control || new FormControl();
        this.ctx = this;
        this.subscription = this.control.valueChanges.debounceTime(500).subscribe((res) => {
            if ( res.length > 0) {
                this.searchFunction(this.target, res).subscribe((data) => {
                    console.log(data);
                    this.data = data.splice(0, 3);
                });
            } else {
                this.data = null;
            }
        });
    }
    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    public viewDetails() {
        console.log('view');
    }
}
