import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'dt-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.scss']
})
export class SelectComponent {
    @Input() public control: FormControl = null;
    @Input() public options: any[] = [];
    @Input() public label: string = '';
    @Input() public placeholder: string = '';
    @Input() public name: string = '';
}
