import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'dt-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent {
    @Input() public control: FormControl = null;
    @Input() public placeholder: string;
    @Input() public type: string = 'text';
    @Input() public name: string;
    @Input() public label: string;
    @Input() public options = [];
    @Input() public mask: any;
}
