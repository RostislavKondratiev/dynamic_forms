import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerDataSource } from '../util/components/table/ds/server.dataSource';

@Component({
    selector: 'dt-deals',
    templateUrl: './deals.component.html',
    styleUrls: ['./deals.component.scss']
})
export class DealsComponent {
    public tableConfig;
    constructor(private http: HttpClient) {
        this.tableConfig = {
            filters: [
                {
                    sortableKey: 'status',
                    type: 'select',
                    name: 'select',
                    label: 'Select status',
                    options: [{
                        label: 'Available',
                        value: 'available'
                    }, {
                        label: 'Unavailable',
                        value: 'unavailable'
                    }]
                },
                {
                    sortableKey: 'city',
                    type: 'text',
                    name: 'name',
                    label: 'Enter City'
                }
            ],
            headers: ['property', 'price', 'city', 'phone', 'status'],
            datasource: new ServerDataSource(this.http, 'deals', {page: 1, limit: 3, status: false}, {status: true}),
            route: 'deals',
        };
    }
}
