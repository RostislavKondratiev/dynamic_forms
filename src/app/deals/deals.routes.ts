import { Routes } from '@angular/router';
import { DealsComponent } from './deals.component';

export const ROUTES: Routes = [
    {path: 'deals', component: DealsComponent}
];
