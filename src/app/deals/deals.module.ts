import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UtilModule } from '../util/util.module';
import { ROUTES } from './deals.routes';
import { DealsComponent } from './deals.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTES),
        UtilModule
    ],
    declarations: [
        DealsComponent
    ],
    providers: [],
    exports: [
        RouterModule
    ]
})
export class DealsModule {
}
