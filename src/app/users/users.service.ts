import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiHttpConfig } from '../util/helpers/apiHttpConfig';

@Injectable()
export class UsersService {
    constructor(private http: HttpClient) {};
    public search(target, value) {
        return this.http.get(`${ApiHttpConfig.users}?${target}_like=${value}`);
    }
}