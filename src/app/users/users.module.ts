import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UtilModule } from '../util/util.module';
import { ROUTES } from './users.routes';
import { UsersComponent } from './users.component';
import { UsersService } from './users.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ROUTES),
        UtilModule
    ],
    declarations: [
        UsersComponent
    ],
    providers: [
        UsersService
    ],
    exports: [
        RouterModule
    ]
})
export class UsersModule {
}
