import { Routes } from '@angular/router';
import { UsersComponent } from './users.component';

export const ROUTES: Routes = [
    {path: '', pathMatch: 'full', redirectTo: 'users'},
    {path: 'users', component: UsersComponent}
];
