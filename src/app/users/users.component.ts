import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { ServerDataSource } from '../util/components/table/ds/server.dataSource';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './users.service';
import { QueryesService } from '../util/components/table/ds/queryes.service';
import { TestService } from '../util/helpers/test.service';

@Component({
    selector: 'dt-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements AfterViewInit {
    @ViewChild('searchTemp') public searchTemp;
    public tableConfig;
    public secondConfig;
    public typeaheadConfig;
    public customTemplate;
    constructor(private http: HttpClient, private usersService: UsersService, private test: TestService) {
        this.typeaheadConfig = {
            placeholder: 'Find by Username',
            target: 'website',
            name: 'search',
            label: 'Search',
            style: {
                width: '300px',
                margin: '10px 0 10px 20px'
            },
            searchFunction: this.usersService.search.bind(this)
        };
        this.tableConfig = {
            filters: [
                {
                    sortableKey: 'status',
                    type: 'select',
                    name: 'select',
                    label: 'Select status',
                    options: [{
                        label: 'Confirmed',
                        value: 'confirmed'
                    }, {
                        label: 'Rejected',
                        value: 'rejected'
                    }]
                },
                {
                    sortableKey: 'username_like',
                    type: 'text',
                    name: 'name',
                    label: 'Enter Name'
                },
            ],
            headers: ['name', 'username', 'email', 'phone', 'website', 'status'],
             // http provider, endpoint, pagination config, sort config, queryParams, filters value
            datasource: new ServerDataSource(this.http, 'users', {page: 1, limit: 5, status: true}, {status: true}, false),
            route: 'users',
        };
        this.secondConfig = {
            filters: [
                {
                    sortableKey: 'status',
                    type: 'select',
                    name: 'select',
                    label: 'Select status',
                    options: [
                        {
                        label: 'Clear',
                        value: '',
                    }, {
                        label: 'Available',
                        value: 'available'
                    }, {
                        label: 'Unavailable',
                        value: 'unavailable'
                    }]
                },
                {
                    sortableKey: 'city',
                    type: 'text',
                    name: 'name',
                    label: 'Enter City'
                }
            ],
            headers: ['property', 'price', 'city', 'phone', 'status'],
            datasource: new ServerDataSource(this.http, 'deals', {page: 1, limit: 3, status: true}, {status: true}, false),
            route: 'users',
        };
        // this.tableConfig = new TableConfig(
        //     [new TableFilter()]
        // )
    }
    public addFilter() {
        this.tableConfig.filters.push({
            sortableKey: 'email',
            type: 'text',
            name: 'email',
            label: 'Enter Email'
        });
        this.tableConfig = Object.assign({}, this.tableConfig);
    }
    public ngAfterViewInit() {
        let test = new QueryesService();
        let test2 = new QueryesService();
        console.log(test === test2);
        console.log('After');
        this.customTemplate = this.searchTemp;
    }
}
